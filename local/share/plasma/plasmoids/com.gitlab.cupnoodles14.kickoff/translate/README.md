# Translations

- Spanish translation: Carlos Barraza

`build`, `merge` and `plasmalocaletest` by Zren (taken from [plasma-applet-eventcalendar](https://github.com/Zren/plasma-applet-eventcalendar/tree/master/package/translate))

## Translation status

|  Locale  |  Lines  | % Done|
|----------|---------|-------|
| Template |      54 |       |
| es       |   53/54 |   98% |
