# Translation of kickoff in LANGUAGE
# Copyright (C) 2021
# This file is distributed under the same license as the kickoff package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: kickoff\n"
"Report-Msgid-Bugs-To: https://gitlab.com/cupnoodles14/plasma-kickoff-grid\n"
"POT-Creation-Date: 2021-11-17 07:21+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../metadata.desktop
msgid "Application Launcher (Favorites in a Grid)"
msgstr ""

#: ../metadata.desktop
msgid "Launcher to start applications"
msgstr ""

#: ../contents/config/config.qml
msgid "General"
msgstr ""

#: ../contents/config/config.qml ../contents/ui/ConfigButtons.qml ../contents/ui/FullRepresentation.qml
msgid "Favorites"
msgstr ""

#: ../contents/config/config.qml ../contents/ui/ConfigButtons.qml ../contents/ui/FullRepresentation.qml
msgid "Applications"
msgstr ""

#: ../contents/config/config.qml
msgid "Advanced"
msgstr ""

#: ../contents/ui/ApplicationsView.qml
msgid "All Applications"
msgstr ""

#: ../contents/ui/ApplicationsView.qml
msgid "Applications updated."
msgstr ""

#: ../contents/ui/code/tools.js
msgid "Remove from Favorites"
msgstr ""

#: ../contents/ui/code/tools.js
msgid "Add to Favorites"
msgstr ""

#: ../contents/ui/code/tools.js
msgid "On All Activities"
msgstr ""

#: ../contents/ui/code/tools.js
msgid "On the Current Activity"
msgstr ""

#: ../contents/ui/code/tools.js
msgid "Show in Favorites"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "List views icon size:"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml ../contents/ui/ConfigGridView.qml
msgid "Small"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml ../contents/ui/ConfigGridView.qml
msgid "Large"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "Hide subtitles in list views"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "Horizontal inner margins:"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "Vertical inner margins:"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "Horizontal outer margins:"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "Vertical outer margins:"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "Resize the menu using the mouse (Meta + RMB drag by default)"
msgstr ""

#: ../contents/ui/ConfigAdvanced.qml
msgid "Menu width/height:"
msgstr ""

#: ../contents/ui/ConfigApplications.qml
msgid "Use the same parameters as Favorites tab"
msgstr ""

#: ../contents/ui/ConfigButtons.qml ../contents/ui/FullRepresentation.qml
msgid "Computer"
msgstr ""

#: ../contents/ui/ConfigButtons.qml ../contents/ui/FullRepresentation.qml
msgid "History"
msgstr ""

#: ../contents/ui/ConfigButtons.qml ../contents/ui/FullRepresentation.qml
msgid "Often Used"
msgstr ""

#: ../contents/ui/ConfigButtons.qml ../contents/ui/FullRepresentation.qml
msgid "Leave"
msgstr ""

#: ../contents/ui/ConfigButtons.qml
msgid "Active Tabs"
msgstr ""

#: ../contents/ui/ConfigButtons.qml
msgid "Inactive Tabs"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgid "Icon:"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgctxt "@item:inmenu Open icon chooser dialog"
msgid "Choose..."
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgctxt "@item:inmenu Reset icon to default"
msgid "Clear Icon"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgid "General:"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgid "Switch tabs on hover"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgid "Show applications by name"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgid "Configure enabled search plugins"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgid "Sort alphabetically"
msgstr ""

#: ../contents/ui/ConfigGeneral.qml
msgid "Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging."
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Flow:"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Rows"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Columns"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Maximum columns on screen:"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Always use maximum columns"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Icon size:"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Label position:"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Right"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Bottom"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Hidden"
msgstr ""

#: ../contents/ui/ConfigGridView.qml
msgid "Display subtitles"
msgstr ""

#: ../contents/ui/Header.qml
msgid "%2@%3 (%1)"
msgstr ""

#: ../contents/ui/Header.qml
msgid "%1@%2"
msgstr ""

#: ../contents/ui/Header.qml
msgid "Search..."
msgstr ""

#: ../contents/ui/Kickoff.qml
msgid "Edit Applications..."
msgstr ""
