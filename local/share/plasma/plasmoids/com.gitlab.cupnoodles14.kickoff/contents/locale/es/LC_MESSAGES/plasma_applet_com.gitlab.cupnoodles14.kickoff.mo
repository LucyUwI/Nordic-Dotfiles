��    6      �  I   |      �     �  
   �  /   �  -   �               -     6     G  *   b     �     �     �     �     �     �  W   �     2  	   G     Q     W     _     h     o     �     �     �  
   �     �     �     �     �     �          "     8     S  
   f     q     �     �  <   �     �     �  	   �               /     5     I  (   ^     �     �  O  �     
  
   
  
   
     #
     5
     G
     [
     d
  "   {
  6   �
     �
     �
     �
       
          A   *     l  	   �     �     �     �     �     �  	   �      �  !   �                    2     K  "   R     u  *   {     �     �  	   �     �     �       S   '     {     �  	   �      �     �     �     �  $   �  4        E     e        5      6          &   2             "           %       (   	                          0   3              -                            1                    
   !   ,   .                     #          4      '   /                $      *   +         )       %1@%2 %2@%3 (%1) @item:inmenu Open icon chooser dialogChoose... @item:inmenu Reset icon to defaultClear Icon Active Tabs Add to Favorites Advanced All Applications Always use maximum columns Application Launcher (Favorites in a Grid) Applications Applications updated. Bottom Columns Computer Display subtitles Drag tabs between the boxes to show/hide them, or reorder the visible tabs by dragging. Edit Applications... Favorites Flow: General General: Hidden Hide subtitles in list views History Horizontal inner margins: Horizontal outer margins: Icon size: Icon: Inactive Tabs Label position: Large Launcher to start applications Leave List views icon size: Maximum columns on screen: Menu width/height: Often Used On All Activities On the Current Activity Remove from Favorites Resize the menu using the mouse (Meta + RMB drag by default) Right Rows Search... Show applications by name Show in Favorites Small Sort alphabetically Switch tabs on hover Use the same parameters as Favorites tab Vertical inner margins: Vertical outer margins: Project-Id-Version: kickoff
Report-Msgid-Bugs-To: https://gitlab.com/cupnoodles14/plasma-kickoff-grid
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Carlos Barraza <carlosbarrazaes@gmail.com>
Language-Team: Español
Language: Español
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 %1@%2 %2@%3 (%1) Escoger... Restablecer Icono Pestañas Activas Agregar a Favoritos Avanzado Todas las Aplicaciones Siempre usar el maximo de columnas Lanzador de Aplicaciones (Favoritos en una Cuadricula) Aplicaciones Aplicaciones actualizadas. Abajo Columnas Computador Mostrar Subtitulos Arrastra las pestañas para mostrarlas, ocultarlas o reordenarlas Editar Aplicación... Favoritos Orden: General General: Oculto Ocultar los subtitulos Historial Margenes interiores horizontales Margenes exteriores horizontales: Tamaño Icono: Pestañas Inactivas Posicion de la etiqueta: Grande Lanzador para iniciar aplicaciones Salir Tamaño de los iconos de la vista de lista Maximo de columnas en pantalla: Menú ancho/alto Mas Usado En todas las Actividades En la Actividad actual Eliminar de Favoritos Redimencionar el menú usando el ratón (Meta + Clic derecho arrastrar por defecto) Derecha Filas Buscar... Mostrar aplicaciones por nombres Mostrar en Favoritos Pequeño Ordenar Alfabéticamente Cambiar pestañas al pasar el ratón Usar los mismos parametros que la pestaña Favoritos Margenes interiores verticales: Margenes exteriores verticales: 