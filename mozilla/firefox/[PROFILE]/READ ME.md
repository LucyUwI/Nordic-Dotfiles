***For this Firefox theme to work:***

1. Go to `about:config`
2. Search `toolkit.legacyUserProfileCustomizations.stylesheets` and set to `true`
3. Search `browser.uidensity` and set to `1`
4. Open you profile folder (Go to `about:profiles` and find the profile that `Default Profile` is set to `yes`, and click `Open Directory` for `Root Directory`)
5. Copy the `chrome` folder into your profile
